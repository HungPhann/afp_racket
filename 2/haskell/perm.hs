import Data.Sequence as S
import Data.List.Split

main = do 
    x <- getLine
    y <- getLine
    let 
        input_queue = makeQueue $ foldl (\acc a -> ((read::String->Int) a) : acc ) [] (splitOn " " x)
        output_queue = makeQueue $ foldl (\acc a -> ((read::String->Int) a) : acc ) [] (splitOn " " y)
        flag = perm input_queue output_queue emptyStack

    print flag
    

inner_loop :: Seq Int -> Seq Int -> (Seq Int, Seq Int)
inner_loop output_queue stack = if isEmptyStack stack then
                                    (output_queue, stack)
                                else
                                    let
                                        peekStackValue = peekStack stack
                                        peekOutputQueueValue = peekQueue output_queue
                                    in
                                        if peekStackValue == peekOutputQueueValue then
                                            let
                                                (newOutputQueue, popInputQueueValue) = deQueue output_queue
                                                (newStack, popStackValue) = popStack stack
                                            in
                                                inner_loop newOutputQueue newStack
                                        else 
                                            (output_queue, stack)


perm :: Seq Int -> Seq Int -> Seq Int -> Bool
perm input_queue output_queue stack =   if isEmptyQueue input_queue then
                                            if isEmptyStack stack then
                                                True
                                            else
                                                False
                                        else
                                            let 
                                                (newInputQueue, popInputQueueValue) = deQueue input_queue
                                                peekOutputQueueValue = peekQueue output_queue
                                            in
                                                if popInputQueueValue /= peekOutputQueueValue then
                                                    perm newInputQueue output_queue (pushStack stack popInputQueueValue)
                                                else 
                                                    let 
                                                        (popOutputQueue, popOutputQueueValue) = deQueue output_queue
                                                        (newOutputQueue, newStack) = inner_loop popOutputQueue stack
                                                    in 
                                                        perm newInputQueue newOutputQueue newStack
            


emptyStack :: Seq a
emptyStack = S.empty

isEmptyStack :: Seq a -> Bool
isEmptyStack s = S.null s

pushStack :: Seq a -> a -> Seq a
pushStack s x =  x <| s

popStack :: Seq a -> (Seq a, a)
popStack s = (S.drop 1 s, S.index s 0)

peekStack :: Seq a -> a
peekStack s = S.index s 0


makeQueue :: [a] -> Seq a
makeQueue xs = S.fromList xs

isEmptyQueue :: Seq a -> Bool
isEmptyQueue q = S.null q

enQueue :: Seq a -> a -> Seq a
enQueue q x = q |> x

deQueue :: Seq a -> (Seq a, a)
deQueue q = (S.drop 1 q, S.index q 0)

peekQueue :: Seq a -> a
peekQueue q = S.index q 0
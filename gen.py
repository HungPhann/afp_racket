from random import randint

size = 10000

numbers = []

for i in range(size):
    numbers.append(randint(0, 10000000000))

s = ""

for number in numbers:
    s += str(number) + " "

s = s.strip()

f = open("10000-t-task-3.txt", "w")
f.write(s)
f.write("\n")
f.write(s)
f.close()    


